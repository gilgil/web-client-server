# web-client-server

### Setting IP(foo.com)

* Add foo.com IP to 127.0.0.1 modifying /etc/hosts file.
```
127.0.0.1 foo.com
```

###

### TCP test
* Add comment(client.cpp server.cpp), build client and server, and test TCP communication.
```
// #define USE_SSL
```

### SSL test
* Remove comment(client.cpp server.cpp), build client and server, and test SSL communication.
```
#define USE_SSL
```
* Register root.crt file a as root CA and test again.
```
$ sudo apt-get install -y ca-certificates
$ sudo cp root.crt /usr/local/share/ca-certificates
$ sudo update-ca-certificates
```

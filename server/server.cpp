#include <QCoreApplication>

// #define USE_SSL

#ifndef USE_SSL
#include <QTcpServer>
#else
#include <QFile>
#include <QSslKey>
#include <QSslServer>
#endif
#include "obj.h"

int main(int argc, char *argv[]) {
	QCoreApplication a(argc, argv);

#ifndef USE_SSL
	QTcpServer server;
#else
	QSslServer server;

	QSslConfiguration sslConfiguration(QSslConfiguration::defaultConfiguration());

	QFile keyFile("foo.com.key");
	if (!keyFile.open(QIODevice::ReadOnly)) {
		qDebug() << "can not open key file" << keyFile.errorString();
		exit(-1);
	}
	QSslKey sslKey(&keyFile, QSsl::Rsa);
	sslConfiguration.setPrivateKey(sslKey);
	keyFile.close();

	QFile crtFile("foo.com.crt");
	if (!crtFile.open(QIODevice::ReadOnly)) {
		qDebug() << "can not open crt file" << crtFile.errorString();
		exit(-1);
	}
	QSslCertificate sslCertificate(&crtFile);
	sslConfiguration.setLocalCertificate(sslCertificate);
	crtFile.close();

	server.setSslConfiguration(sslConfiguration);
#endif
	Obj obj;

	QObject::connect(&server, &QTcpServer::newConnection, &obj, &Obj::doNewConnection);
	QObject::connect(&server, &QTcpServer::pendingConnectionAvailable, &obj, &Obj::doPendingConnectionAvailable);

	bool ok = true;
#ifndef USE_SSL
	ok = server.listen(QHostAddress::Any, 80);
#else
	ok = server.listen(QHostAddress::Any, 443);
#endif
	if (!ok) {
		qDebug() << server.errorString();
		exit(-1);
	}

	return a.exec();
}

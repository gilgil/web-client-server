#include "obj.h"
#include <QDebug>

void Obj::doNewConnection() {
	qDebug() << "doNewConnection";
	QTcpServer* server = dynamic_cast<QTcpServer*>(sender());
	Q_ASSERT(server != nullptr);

	while (server->hasPendingConnections()) {
		QTcpSocket* socket = server->nextPendingConnection();
		QObject::connect(socket, &QTcpSocket::disconnected, this, &Obj::doDisconnected);
		QObject::connect(socket, &QTcpSocket::errorOccurred, this, &Obj::doErrorOccurred);
		QObject::connect(socket, &QTcpSocket::readyRead, this, &Obj::doReadyRead);
	}
}

void Obj::doPendingConnectionAvailable() {
	qDebug() << "doPendingConnectionAvailable";
	doNewConnection();
}

void Obj::doDisconnected() {
	qDebug() << "doDisconnected";
}

void Obj::doErrorOccurred(QAbstractSocket::SocketError socketError) {
	qDebug() << socketError;
}

void Obj::doReadyRead() {
	qDebug() << "doReadyRead";
	QTcpSocket* socket = dynamic_cast<QTcpSocket*>(sender());
	Q_ASSERT(socket != nullptr);
	qDebug() << socket->readAll();
	socket->write("HTTP/1.1 200 Ok\r\nContent-Length: 5\r\n\r\nOK!!!!!");
}

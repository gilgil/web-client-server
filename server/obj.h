#pragma once

#include <QTcpServer>
#include <QTcpSocket>

struct Obj : QObject {
	Q_OBJECT

public slots:
	//
	// Server
	//
	void doNewConnection();
	void doPendingConnectionAvailable();

	//
	// Socket
	//
	// void doConnected(); // not reachable
	void doDisconnected();
	void doErrorOccurred(QAbstractSocket::SocketError socketError);
	void doReadyRead();
};

#pragma once

#include <QTcpSocket>

struct Obj : QObject {
	Q_OBJECT

public slots:
	//
	// Socket
	//
	void doConnected();
	void doDisconnected();
	void doErrorOccurred(QAbstractSocket::SocketError socketError);
	void doReadyRead();
};

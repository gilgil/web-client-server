#include <QCoreApplication>

// #define USE_SSL

#ifndef USE_SSL
#include <QTcpSocket>
#else
#include <QSslSocket>
#endif
#include "obj.h"

int main(int argc, char *argv[]) {
	QCoreApplication a(argc, argv);

#ifndef USE_SSL
	QTcpSocket socket;
#else
	QSslSocket socket;
#endif
	Obj obj;

	QObject::connect(&socket, &QTcpSocket::connected, &obj, &Obj::doConnected);
	QObject::connect(&socket, &QTcpSocket::disconnected, &obj, &Obj::doDisconnected);
	QObject::connect(&socket, &QTcpSocket::errorOccurred, &obj, &Obj::doErrorOccurred);
	QObject::connect(&socket, &QTcpSocket::readyRead, &obj, &Obj::doReadyRead);

#ifndef USE_SSL
	socket.connectToHost("foo.com", 80);
#else
	socket.connectToHostEncrypted("foo.com", 443);
#endif

	return a.exec();
}

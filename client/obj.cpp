#include "obj.h"
#include <QDebug>

void Obj::doConnected() {
	qDebug() << "doConnected";
	QTcpSocket* socket = dynamic_cast<QTcpSocket*>(sender());
	Q_ASSERT(socket != nullptr);
	socket->write("GET / HTTP/1.1\r\nHost: foo.com\r\n\r\n");
}

void Obj::doDisconnected() {
	qDebug() << "doDisconnected";
	exit(-1);
}

void Obj::doErrorOccurred(QAbstractSocket::SocketError socketError) {
	qDebug() << socketError;
	exit(-1);
}

void Obj::doReadyRead() {
	qDebug() << "doReadyRead";
	QTcpSocket* socket = dynamic_cast<QTcpSocket*>(sender());
	Q_ASSERT(socket != nullptr);
	qDebug() << socket->readAll();
}
